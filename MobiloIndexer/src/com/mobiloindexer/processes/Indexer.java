package com.mobiloindexer.processes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

/**
 * This class is the starting point and abstracts out the indexing work
 * @author Soumadri Roy
 *
 */
public class Indexer {
	private final static Logger LOGGER = Logger.getLogger(Indexer.class);
	private final ResourceBundle miResourceBundle = ResourceBundle.getBundle("com.mobiloindexer.resources.mobiloindexer");
	final String PATH_SEPERATOR = System.getProperty("file.separator");
	final String INDEX_EXTN = miResourceBundle.getString("sys.indexext");
	String zipFilePath = null;
	String indexOutputDir = null;
	
	/**
	 * Constructor for indexer
	 * @param zipFilePath The input zip file with all the documents to be indexed
	 * @param indexOutputDir The output directory to dump the index files to
	 */
	public Indexer(String zipFilePath, String indexOutputDir) {
		this.zipFilePath = zipFilePath;
		this.indexOutputDir = indexOutputDir;					
	}
	
	/**
	 * Starts the indexing process
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws ClassNotFoundException
	 */
	public void index() throws FileNotFoundException, IOException, NoSuchAlgorithmException, ClassNotFoundException {
		//Gather the stop-word list
		String [] stopwords = miResourceBundle.getString("stopwordlist").split(",");
		ArrayList<String> stopwordlist = new ArrayList<String>();
		
		for(String eachstopword:stopwords){
			stopwordlist.add(eachstopword);
		}
		
		//Open the zip file
		File file = new File(zipFilePath);
		ZipFile zipfile = new ZipFile(file);
		ZipEntry entry = null;
		InputStream currentfile = null;
		ByteArrayOutputStream contentinbytes = null;
		byte[] bytes = null;
		String filename = null;
		String [] fileuritokens = null;
		
		//Initialize the tokenizer
		Tokenizer tokengen = new Tokenizer(stopwordlist, indexOutputDir);
		
		//Process each document in the zip
		for (Enumeration e = zipfile.entries(); e.hasMoreElements(); ) {

			entry = (ZipEntry) e.nextElement();
			if(!entry.isDirectory()){

				LOGGER.info("Processing document: "+entry.getName());
				LOGGER.debug("File name: " + entry.getName()+ "; size: " + entry.getSize()+ "; compressed size: "+ entry.getCompressedSize());
				
				currentfile = zipfile.getInputStream(entry);
				contentinbytes = new ByteArrayOutputStream();
				IOUtils.copy(currentfile, contentinbytes);
				bytes = contentinbytes.toByteArray();
															
				fileuritokens = entry.getName().split("/");
				filename = fileuritokens[fileuritokens.length-1].substring(0, fileuritokens[fileuritokens.length-1].indexOf("."));

				LOGGER.debug("Token generation started...");
				
				//Tokenizing the input xml file
				tokengen.tokenize(new String(bytes), filename);
				
				LOGGER.debug("Token generation finished");				
				LOGGER.debug("Processing for the document "+entry.getName()+" finished");
			}	
		}
		
		LOGGER.debug("Index merging started");
		
		//Partition and merge the per document indexes into a single one
		IndexMerger generateindex = new IndexMerger(new File(indexOutputDir + PATH_SEPERATOR + "index" + INDEX_EXTN), indexOutputDir);
		generateindex.mergeInMemory();
		
		LOGGER.debug("Index merging finished");
		
		zipfile.close();
	}
}
