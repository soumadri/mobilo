package com.mobiloindexer.processes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mobiloindexer.domain.DocumentRefComparator;
import com.mobiloindexer.domain.DocumentReference;
import com.mobiloindexer.domain.PostingListReference;

public class IndexMerger {
	File fOutput;
	String indexfolder;
	final int partitionSize = 1000;
	
	private final static Logger LOGGER = Logger.getLogger(IndexMerger.class);
	private final String PATH_SEPERATOR = System.getProperty("file.separator");
	private final ResourceBundle miResourceBundle = ResourceBundle.getBundle("com.mobiloindexer.resources.mobiloindexer");
	
	public IndexMerger(File fOutput,String indexfolder ) throws FileNotFoundException, IOException{		
		this.fOutput = fOutput;
		this.indexfolder = indexfolder;
		
	}

	public void mergeInMemory() throws FileNotFoundException, IOException, ClassNotFoundException{
		final String INDEX_EXTN = miResourceBundle.getString("sys.indexext");
		File folder = new File(indexfolder);	//Input directory holding all document's indexes
		LinkedList<DocumentReference> postinglist = null;
		
		//Final merged hashmap
		HashMap<Integer, LinkedList<DocumentReference>> finalindexhm = new HashMap<Integer, LinkedList<DocumentReference>>();
		
		LOGGER.info("Started merging...");
		
		DocumentRefComparator comparator = new DocumentRefComparator();
		
		//For index of each document
		for (File fileEntry : folder.listFiles()){	
			LOGGER.info(fileEntry.getName());
			//Deserialize the index to object
			HashMap<Integer, DocumentReference> deserializedcurrentobject = (HashMap<Integer, DocumentReference>) IndexUtils.readIndex(fileEntry.getAbsolutePath());
			Set<Entry<Integer, DocumentReference>> currentfileentryset = deserializedcurrentobject.entrySet();
			Iterator<Entry<Integer, DocumentReference>> currentfileentrysetitr = currentfileentryset.iterator(); 
					
			int currenttoken;
			
			//Iterate over each token in the index and compare
			while(currentfileentrysetitr.hasNext()){
				currenttoken = currentfileentrysetitr.next().getKey();

				//If the merged index has the current token, then update the posting list
				if( finalindexhm.get(currenttoken) != null){
					LinkedList<DocumentReference> tmpLL = finalindexhm.get(currenttoken);
					tmpLL.add(deserializedcurrentobject.get(currenttoken));
					Collections.sort(tmpLL,comparator);
				}else{
					//Otherwise, create a new PostingList with the document ref. object
					postinglist = new LinkedList<DocumentReference>();
					postinglist.add((DocumentReference)deserializedcurrentobject.get(currenttoken));
					Collections.sort(postinglist,comparator);
					finalindexhm.put(currenttoken, postinglist);
				}
			}			
		}
		LOGGER.info("Index merging finished");
		
		//Delete temporary forward indexes
		LOGGER.info("Deleting temporary indexes");
		IndexUtils.deleteTempIndexes(indexfolder);
		
		//Partioning merged index into dictionary and posting lists		
		LOGGER.info("Index partitioning started...");
		
		Set<Entry<Integer, LinkedList<DocumentReference>>> finalindexedset = finalindexhm.entrySet();
		Iterator<Entry<Integer, LinkedList<DocumentReference>>> finalindexeditr = finalindexedset.iterator();
		
		//Dictionary hash map
		HashMap<Integer, PostingListReference> dictionaryhm = new HashMap<Integer, PostingListReference>();
		
		//each posting block hash map
		HashMap<String, LinkedList<DocumentReference>> eachpostingblockhm = new HashMap<String, LinkedList<DocumentReference>>();
		String postingblockid = null ;
		
		int plcount = 1;
		int pbcount = 1;
		int indexsize = finalindexhm.size(); 
		while(finalindexeditr.hasNext()){
			
			Entry<Integer, LinkedList<DocumentReference>> currententry = finalindexeditr.next();
			int currenttoken = currententry.getKey();
			LinkedList<DocumentReference> eachpostinglist =  finalindexhm.get(currenttoken);
			Iterator<DocumentReference> eachpostinglistitr = eachpostinglist.iterator();
			
			int totalhitcount = 0;
			while(eachpostinglistitr.hasNext()){
				
				totalhitcount = totalhitcount+eachpostinglistitr.next().getHitcount();
				
			}
			
			int doccount = eachpostinglist.size();
			String postinglistid = "pl"+plcount ;
			postingblockid = "pb"+pbcount;
			
			//Create a reference from the master index to the posting block
			PostingListReference plref = new PostingListReference();
			plref.setPostinglistid(postinglistid);
			plref.setDoccount(doccount);
			plref.setTotalhitcount(totalhitcount);
			
			
			if(eachpostingblockhm.size() == partitionSize){
								
				IndexUtils.writeIndex(eachpostingblockhm, indexfolder + PATH_SEPERATOR + postingblockid + INDEX_EXTN);
				
				pbcount++;
				postingblockid = "pb"+pbcount;
				eachpostingblockhm = new HashMap<String, LinkedList<DocumentReference>>();
				eachpostingblockhm.put(postinglistid, eachpostinglist);
			}
			else{
				eachpostingblockhm.put(postinglistid, eachpostinglist);
			}
			
			if(plcount == indexsize && eachpostingblockhm.size() != 1000){				
				IndexUtils.writeIndex(eachpostingblockhm, indexfolder + PATH_SEPERATOR + postingblockid + INDEX_EXTN);
			}
			
			
			plref.setPostingblockid(postingblockid);
			dictionaryhm.put(currenttoken, plref);
			plcount++;
		}
		
		//Write the merged index to disk		
		IndexUtils.writeIndex(dictionaryhm, fOutput.getAbsolutePath());
		
        LOGGER.info("Index partitioning finished");
	}
	
}
