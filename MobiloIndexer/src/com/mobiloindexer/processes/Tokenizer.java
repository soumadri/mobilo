package com.mobiloindexer.processes;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import com.mobiloindexer.domain.DocumentReference;
import com.mobiloindexer.domain.Hit;

import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetBeginAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetEndAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.OriginalTextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

/**
 * This class is responsible to tokenize the input text ignoring the specified stopwords
 * @author Soumadri Roy
 *
 */
public class Tokenizer {
	ArrayList<String> stopwordslist=null;
	String outputPath = null;
	private final ResourceBundle miResourceBundle = ResourceBundle.getBundle("com.mobiloindexer.resources.mobiloindexer");
	
	/**
	 * Constructor for the tokenizer
	 * @param stopwordslist List of words to be ignored
	 * @param outputPath Output directory to dump the index files to
	 */
	public Tokenizer(ArrayList<String> stopwordslist, String outputPath){
		this.stopwordslist = stopwordslist;
		this.outputPath = outputPath;
	}
	
	/**
	 * Tokenizes the provided content and writes it to respective index file
	 * @param stringContent	The content to be tokenized
	 * @param filename	The name of the input file
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 */
	public void tokenize(String stringContent, String filename) throws FileNotFoundException, IOException, NoSuchAlgorithmException{
		final String INDEX_EXTN = miResourceBundle.getString("sys.indexext");
		
		 
		//Create the document ID to be use in search index.
		//This is an MD5 hash of the document filename.
		
		/*MessageDigest m=MessageDigest.getInstance("MD5");
		m.update(filename.getBytes());
		BigInteger filenamehash = new BigInteger(1, m.digest());
		String hashedfilename = filenamehash.toString(16);*/
		
		int hasedFilename = IndexUtils.hashInt(filename);
		
		//The index dictionary
		HashMap<Integer, DocumentReference> hm = new HashMap<Integer, DocumentReference>();
		
		//Creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution 
		Properties props = new Properties();
		//props.put("annotators", "tokenize, cleanxml, ssplit, pos, lemma, ner, parse, dcoref");
		props.put("annotators", "tokenize, cleanxml, ssplit");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		//Create an empty Annotation just with the given text
		Annotation document = new Annotation(stringContent);

		//Run all Annotators on this text
		pipeline.annotate(document);

		//These are all the sentences in this document
		//a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		//Create the posting list
		DocumentReference currentdoc = null;
		LinkedList<Hit> hitlist= null;
		int hitcount;
		boolean newtoken = true;
		Stemmer s = new Stemmer();

		for(CoreMap sentence: sentences) {
			//Traversing the words in the current sentence
			//a CoreLabel is a CoreMap with additional token-specific methods
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {

				//This is the text of the token
				//String word = token.get(LemmaAnnotation.class);
				String originalword = token.get(OriginalTextAnnotation.class);
				String nlpword = token.get(TextAnnotation.class);
				s.add(originalword);
				s.stem();
				
				String stemmedword = s.toString();		//Find the stem of the tokenized word			
				
				//Ignore the stopwords
				if(!stopwordslist.contains(nlpword)){
					Integer startpos = token.get(CharacterOffsetBeginAnnotation.class);
					Integer endpos = token.get(CharacterOffsetEndAnnotation.class);
					
					//Record a hit
					Hit h = new Hit();
					h.setStartposition(startpos);
					h.setEndposition(endpos);					

					Set<Entry<Integer, DocumentReference>> set = hm.entrySet();
					Iterator<Entry<Integer, DocumentReference>> i = set.iterator(); 
					String stemmed = stemmedword.toLowerCase();
					
					while(i.hasNext()) { 
						Entry<Integer, DocumentReference> me = i.next(); 
						if(me.getKey().equals(IndexUtils.hash(stemmed)))
							newtoken = false;
					}
					
					//If it is a new token then add a new entry 
					//otherwise update the existing list
					if(newtoken){
						hitlist = new LinkedList<Hit>();
						hitlist.add(h);
						hitcount = 1;
						currentdoc = new DocumentReference();
						currentdoc.setHitlist(hitlist);
						currentdoc.setDocid(hasedFilename);
						currentdoc.setDocName(filename);
						currentdoc.setHitcount(hitcount);
						hm.put(IndexUtils.hash(stemmed), currentdoc);
					}else{
						currentdoc = hm.get(IndexUtils.hash(stemmed));
						currentdoc.getHitlist().add(h);
						currentdoc.setHitcount(currentdoc.getHitcount()+1);
						newtoken=true;
					}
				}				
			}
		}		
        
        //Write the per document index
        IndexUtils.writeIndex(hm, outputPath + "~" + hasedFilename + INDEX_EXTN);
				
	}
	

}
