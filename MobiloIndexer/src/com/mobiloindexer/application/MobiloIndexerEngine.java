package com.mobiloindexer.application;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import com.mobiloindexer.processes.Indexer;


public class MobiloIndexerEngine {
	final static String PATH_SEPERATOR = System.getProperty("file.separator");
	private final static Logger LOGGER = Logger.getLogger(MobiloIndexerEngine.class);
	private final static ResourceBundle miResourceBundle = ResourceBundle.getBundle("com.mobiloindexer.resources.mobiloindexer");

	public static void main(String[] args) {
		try{						
			LOGGER.info("Initializing indexer...");
						
			//Check argument presence
			if(args.length < 2) {
				System.out.println(miResourceBundle.getString("msg.usage"));
				System.exit(0);
			}
			
			String zipFilePath = args[0];
			String indexOutputPath = args[1];
			
			if(indexOutputPath==""){
				System.out.println(miResourceBundle.getString("msg.nooutput"));
				System.exit(0);
			} else{
				//Create a temp. directory for the indexing work
				indexOutputPath = (indexOutputPath.endsWith(PATH_SEPERATOR)) ? indexOutputPath + "tmp" + Math.random() + PATH_SEPERATOR : indexOutputPath + PATH_SEPERATOR + "tmp" + Math.random() + PATH_SEPERATOR;
				File outDir = new File(indexOutputPath);
				if(!outDir.exists()) {
					LOGGER.debug("Creating output directories: "+indexOutputPath);
					outDir.mkdirs();
				}
			}
									
			LOGGER.info("Document indexing started...");
			
			//Initialize and start the indexer
			Indexer indexer = new Indexer(zipFilePath, indexOutputPath);
			indexer.index();

			LOGGER.info("Indexing completed");			

		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}
