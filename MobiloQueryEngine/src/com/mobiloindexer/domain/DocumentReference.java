package com.mobiloindexer.domain;

import java.io.Serializable;
import java.util.LinkedList;

public class DocumentReference implements Serializable, Comparable<DocumentReference>{
	
	String docid;
	int hitcount;
	LinkedList<Hit> hitlist;
	
	
	public int getHitcount() {
		return hitcount;
	}
	public void setHitcount(int hitcount) {
		this.hitcount = hitcount;
	}
	public String getDocid() {
		return docid;
	}
	public void setDocid(String docid) {
		this.docid = docid;
	}
	public LinkedList<Hit> getHitlist() {
		return hitlist;
	}
	public void setHitlist(LinkedList<Hit> hitlist) {
		this.hitlist = hitlist;
	}
	
	@Override
	public int compareTo(DocumentReference o) {
		return 	this.getDocid().compareTo(o.getDocid());
	}
 
}
