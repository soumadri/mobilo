package com.mobiloindexer.domain;

public class SearchResultVO {
	
	/**
	 * the hash of the document name where the match is found
	 */
	public String docId;
	
	/**
	 * start and end offset of the matched result
	 */
	public Hit matchedPosition;

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public Hit getMatchedPosition() {
		return matchedPosition;
	}

	public void setMatchedPosition(Hit matchedPosition) {
		this.matchedPosition = matchedPosition;
	}
}
