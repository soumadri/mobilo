package com.mobiloindexer.domain;

import java.io.Serializable;


public class PostingListReference implements Serializable {

	String postingblockid;
	String postinglistid;
	int totalhitcount;
	int doccount;
	
	
	public String getPostingblockid() {
		return postingblockid;
	}
	public void setPostingblockid(String postingblockid) {
		this.postingblockid = postingblockid;
	}
	public String getPostinglistid() {
		return postinglistid;
	}
	public void setPostinglistid(String postinglistid) {
		this.postinglistid = postinglistid;
	}
	public int getTotalhitcount() {
		return totalhitcount;
	}
	public void setTotalhitcount(int totalhitcount) {
		this.totalhitcount = totalhitcount;
	}
	public int getDoccount() {
		return doccount;
	}
	public void setDoccount(int doccount) {
		this.doccount = doccount;
	}

}
