package com.mobiloqueryengine.core;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class MobiloQueryGrammerLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int AND=4;
    public static final int NAME=5;
    public static final int OR=6;
    public static final int SPACE=7;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public MobiloQueryGrammerLexer() {} 
    public MobiloQueryGrammerLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public MobiloQueryGrammerLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g"; }

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:2:6: ( '(' )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:2:8: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:3:6: ( ')' )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:3:8: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:10:4: ( 'OR' | 'or' )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='O') ) {
                alt1=1;
            }
            else if ( (LA1_0=='o') ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }
            switch (alt1) {
                case 1 :
                    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:10:4: 'OR'
                    {
                    match("OR"); 



                    }
                    break;
                case 2 :
                    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:10:11: 'or'
                    {
                    match("or"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:14:4: ( 'AND' | 'and' )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='A') ) {
                alt2=1;
            }
            else if ( (LA2_0=='a') ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:14:4: 'AND'
                    {
                    match("AND"); 



                    }
                    break;
                case 2 :
                    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:14:12: 'and'
                    {
                    match("and"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "NAME"
    public final void mNAME() throws RecognitionException {
        try {
            int _type = NAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:17:7: ( ( 'a' .. 'z' | 'A' .. 'Z' )* )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:17:10: ( 'a' .. 'z' | 'A' .. 'Z' )*
            {
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:17:10: ( 'a' .. 'z' | 'A' .. 'Z' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0 >= 'A' && LA3_0 <= 'Z')||(LA3_0 >= 'a' && LA3_0 <= 'z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:
            	    {
            	    if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NAME"

    // $ANTLR start "SPACE"
    public final void mSPACE() throws RecognitionException {
        try {
            int _type = SPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:20:4: ( ' ' )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:20:4: ' '
            {
            match(' '); 

            skip();

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "SPACE"

    public void mTokens() throws RecognitionException {
        // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:1:8: ( T__8 | T__9 | OR | AND | NAME | SPACE )
        int alt4=6;
        switch ( input.LA(1) ) {
        case '(':
            {
            alt4=1;
            }
            break;
        case ')':
            {
            alt4=2;
            }
            break;
        case 'O':
            {
            int LA4_3 = input.LA(2);

            if ( (LA4_3=='R') ) {
                int LA4_9 = input.LA(3);

                if ( ((LA4_9 >= 'A' && LA4_9 <= 'Z')||(LA4_9 >= 'a' && LA4_9 <= 'z')) ) {
                    alt4=5;
                }
                else {
                    alt4=3;
                }
            }
            else {
                alt4=5;
            }
            }
            break;
        case 'o':
            {
            int LA4_4 = input.LA(2);

            if ( (LA4_4=='r') ) {
                int LA4_10 = input.LA(3);

                if ( ((LA4_10 >= 'A' && LA4_10 <= 'Z')||(LA4_10 >= 'a' && LA4_10 <= 'z')) ) {
                    alt4=5;
                }
                else {
                    alt4=3;
                }
            }
            else {
                alt4=5;
            }
            }
            break;
        case 'A':
            {
            int LA4_5 = input.LA(2);

            if ( (LA4_5=='N') ) {
                int LA4_11 = input.LA(3);

                if ( (LA4_11=='D') ) {
                    int LA4_14 = input.LA(4);

                    if ( ((LA4_14 >= 'A' && LA4_14 <= 'Z')||(LA4_14 >= 'a' && LA4_14 <= 'z')) ) {
                        alt4=5;
                    }
                    else {
                        alt4=4;
                    }
                }
                else {
                    alt4=5;
                }
            }
            else {
                alt4=5;
            }
            }
            break;
        case 'a':
            {
            int LA4_6 = input.LA(2);

            if ( (LA4_6=='n') ) {
                int LA4_12 = input.LA(3);

                if ( (LA4_12=='d') ) {
                    int LA4_15 = input.LA(4);

                    if ( ((LA4_15 >= 'A' && LA4_15 <= 'Z')||(LA4_15 >= 'a' && LA4_15 <= 'z')) ) {
                        alt4=5;
                    }
                    else {
                        alt4=4;
                    }
                }
                else {
                    alt4=5;
                }
            }
            else {
                alt4=5;
            }
            }
            break;
        case ' ':
            {
            alt4=6;
            }
            break;
        default:
            alt4=5;
        }

        switch (alt4) {
            case 1 :
                // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:1:10: T__8
                {
                mT__8(); 


                }
                break;
            case 2 :
                // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:1:15: T__9
                {
                mT__9(); 


                }
                break;
            case 3 :
                // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:1:20: OR
                {
                mOR(); 


                }
                break;
            case 4 :
                // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:1:23: AND
                {
                mAND(); 


                }
                break;
            case 5 :
                // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:1:27: NAME
                {
                mNAME(); 


                }
                break;
            case 6 :
                // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:1:32: SPACE
                {
                mSPACE(); 


                }
                break;

        }

    }


 

}