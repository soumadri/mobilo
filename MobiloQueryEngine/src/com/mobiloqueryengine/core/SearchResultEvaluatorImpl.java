package com.mobiloqueryengine.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import com.mobiloindexer.domain.DocumentReference;
import com.mobiloindexer.domain.Hit;
import com.mobiloindexer.domain.PostingListReference;
import com.mobiloindexer.processes.Stemmer;

public class SearchResultEvaluatorImpl implements ISearchResultEvaluator{
	static HashMap<String,PostingListReference> SearchIndexDictionery;
	
	static{
		System.out.println("loading static="+System.currentTimeMillis());
		ObjectInputStream ois;
		try {
			//FileInputStream fis = new FileInputStream(new File("C:\\anirudh pc backup\\mobilo\\index.ser"));
			FileInputStream fis = new FileInputStream(new File("D:\\Projects\\Mobilo\\Samples\\Book Sample\\output-index\\index.indx"));
			ois = new ObjectInputStream(fis);
			SearchIndexDictionery = (HashMap<String,PostingListReference>)ois.readObject();
			ois.close();
			fis.close();
			System.out.println("loaded static="+System.currentTimeMillis());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		//SearchIndexMap = getSampleIndexObject();
		
	}

	
	
	@Override
	public LinkedList<DocumentReference> getSearchResults(String keyword) throws IOException, ClassNotFoundException {
		/**
		 * stemming the search keyword
		 */
		Stemmer stemmer = new Stemmer();
		stemmer.add(keyword);
		stemmer.stem();
		String stemmedKeyword = stemmer.toString();
		
		/**
		 * getting the Posting list reference which is used to get the doclist from the fragmented index
		 */
		PostingListReference postingListReference = SearchIndexDictionery.get(stemmedKeyword);
		
		FileInputStream fis = new FileInputStream(new File("D:\\Projects\\Mobilo\\Samples\\Book Sample\\output-index\\"+postingListReference.getPostingblockid()+".indx"));
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		HashMap<String, LinkedList<DocumentReference>> block = (HashMap<String, LinkedList<DocumentReference>>)ois.readObject();
		ois.close();
		fis.close();
		
		/**
		 * getting the doc list for the search keyword
		 */
		LinkedList<DocumentReference> matchedDocList = block.get(postingListReference.getPostinglistid());

		return matchedDocList;
	}

	@Override
	public LinkedList<DocumentReference> intersectResults(
			LinkedList<DocumentReference> resultSet1,
			LinkedList<DocumentReference> resultSet2) {
		
		LinkedList<DocumentReference> intersectList = new LinkedList<DocumentReference>();
		Iterator<DocumentReference> iterator =  resultSet1.iterator();
		
		while(iterator.hasNext()){
			DocumentReference currItem = iterator.next();
			if(resultSet2.contains(currItem)){
				intersectList.add(currItem);
			}
			
		}
		
		return intersectList;
	}

	@Override
	public LinkedList<DocumentReference> unionResults(
			LinkedList<DocumentReference> resultSet1,
			LinkedList<DocumentReference> resultSet2) {
		
		LinkedList<DocumentReference> unionList = new LinkedList<DocumentReference>();
		Iterator<DocumentReference> iterator =  resultSet2.iterator();
		
		while(iterator.hasNext()){
			DocumentReference currItem = iterator.next();
			if(!resultSet1.contains(currItem)){
				unionList.add(currItem);
			}
			
		}
		
		Iterator<DocumentReference> unionIterator = unionList.iterator();
		while(unionIterator.hasNext()){
			resultSet1.add(unionIterator.next());
			
		}
		
		return resultSet1;
	}

	@Override
	public LinkedList<DocumentReference> finalize(
			LinkedList<DocumentReference> resultSet1) {
		
		
		LinkedList<DocumentReference> sortedList = new LinkedList<DocumentReference>();
		
		Boolean initSwitch = true;
		for(DocumentReference curRef:resultSet1){
			if(initSwitch){
				sortedList.add(curRef);
				
				initSwitch = false;
			}else{
				
				insert(sortedList, curRef);
				
				
			}
		}
		
		return sortedList;
	}
	
	public void insert(LinkedList<DocumentReference> sortList, DocumentReference DocumentReference){
		int index = 0;
		int listSize = sortList.size(); 
		while(true){
			
			if(sortList.get(index).getHitcount() > DocumentReference.getHitcount()){
				if((index == (listSize-1)) || (sortList.get(index+1).getHitcount() <= DocumentReference.getHitcount())){
					sortList.add((index+1), DocumentReference);
					break;
				}
			}else{
				sortList.add(index, DocumentReference);
				break;
			}	
			
			index++;
		}
		
		
	}
	
	public static String serializeDocRef(DocumentReference DocumentReference){
		
		StringBuilder resultString = new StringBuilder();
		resultString.append("[\nDocID: " + DocumentReference.getDocid()+"\nNumber of hits: "+DocumentReference.getHitcount() +"\nHits:\n");
		for(Hit hit:DocumentReference.getHitlist()){
			resultString.append("start position: "+hit.getStartposition()+"  end position: "+hit.getEndposition()+"\n");
			
		}
		resultString.append("]\n");
		return resultString.toString();
	}
}
