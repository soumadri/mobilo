package com.mobiloqueryengine.core;

import java.io.IOException;
import java.util.LinkedList;
import java.util.ResourceBundle;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;

import com.mobiloindexer.domain.DocumentReference;

public class MobiloQueryEngine {

	/**
	 * @param args
	 * @throws RecognitionException
	 */
	ISearchResultEvaluator searchResultEvaluator = new SearchResultEvaluatorImpl();
	private final static ResourceBundle miResourceBundle = ResourceBundle.getBundle("com.mobiloqueryengine.resources.mobiloqueryengine");

	public LinkedList<DocumentReference> evaluateQuery(String query)
			throws RecognitionException, IOException, ClassNotFoundException {
		ANTLRStringStream input = new ANTLRStringStream(query);
		TokenStream tokens = new CommonTokenStream(new MobiloQueryGrammerLexer(
				input));

		// parser generates abstract syntax tree
		MobiloQueryGrammerParser parser = new MobiloQueryGrammerParser(tokens);
		MobiloQueryGrammerParser.start_return ret = parser.start();

		// acquire parse result
		CommonTree ast = (CommonTree) ret.tree;
		return evaluateTree(ast);

	}

	private LinkedList<DocumentReference> evaluateTree(CommonTree ast)
			throws IOException, ClassNotFoundException {

		if (ast.getChildren() != null) {
			LinkedList<DocumentReference> left = evaluateTree((CommonTree) ast
					.getChild(0));
			LinkedList<DocumentReference> right = evaluateTree((CommonTree) ast
					.getChild(1));
			return evaluate(ast.getText(), left, right);
		} else {
			//System.out.println("Getting search result for "+ast.getText()+" result size="+searchResultEvaluator.getSearchResults(ast.getText()).size());
			return searchResultEvaluator.getSearchResults(ast.getText());
		}

	}

	private LinkedList<DocumentReference> evaluate(String operator,
			LinkedList<DocumentReference> left,
			LinkedList<DocumentReference> right) {
		
		LinkedList<DocumentReference> result=null;
		
		if(operator.equalsIgnoreCase(miResourceBundle.getString("OR-OPERATOR"))){
			result= searchResultEvaluator.unionResults(left, right);
		//	System.out.println("In or with result="+result.size()+"and input left="+left.size()+" right="+right.size());
		}
		else if(operator.equalsIgnoreCase(miResourceBundle.getString("AND-OPERATOR"))){
			result= searchResultEvaluator.intersectResults(left, right);
		//	System.out.println("In and with result="+result.size()+"and input left="+left.size()+" right="+right.size());
		}
		//System.out.println("Returning result of length"+result.size());
		return result;
	}

}
