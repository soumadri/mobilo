package com.mobiloqueryengine.core;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class MobiloQueryGrammerParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "NAME", "OR", "SPACE", "'('", "')'"
    };

    public static final int EOF=-1;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int AND=4;
    public static final int NAME=5;
    public static final int OR=6;
    public static final int SPACE=7;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public MobiloQueryGrammerParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public MobiloQueryGrammerParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return MobiloQueryGrammerParser.tokenNames; }
    public String getGrammarFileName() { return "D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g"; }


    public static class start_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "start"
    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:23:1: start : andExpr ;
    public final MobiloQueryGrammerParser.start_return start() throws RecognitionException {
        MobiloQueryGrammerParser.start_return retval = new MobiloQueryGrammerParser.start_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        MobiloQueryGrammerParser.andExpr_return andExpr1 =null;



        try {
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:24:2: ( andExpr )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:24:4: andExpr
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_andExpr_in_start117);
            andExpr1=andExpr();

            state._fsp--;

            adaptor.addChild(root_0, andExpr1.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "start"


    public static class andExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "andExpr"
    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:27:1: andExpr : ( orExpr ) ( ( AND ^ orExpr )* ) ;
    public final MobiloQueryGrammerParser.andExpr_return andExpr() throws RecognitionException {
        MobiloQueryGrammerParser.andExpr_return retval = new MobiloQueryGrammerParser.andExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token AND3=null;
        MobiloQueryGrammerParser.orExpr_return orExpr2 =null;

        MobiloQueryGrammerParser.orExpr_return orExpr4 =null;


        CommonTree AND3_tree=null;

        try {
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:28:2: ( ( orExpr ) ( ( AND ^ orExpr )* ) )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:28:4: ( orExpr ) ( ( AND ^ orExpr )* )
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:28:4: ( orExpr )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:28:5: orExpr
            {
            pushFollow(FOLLOW_orExpr_in_andExpr129);
            orExpr2=orExpr();

            state._fsp--;

            adaptor.addChild(root_0, orExpr2.getTree());

            }


            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:28:13: ( ( AND ^ orExpr )* )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:28:14: ( AND ^ orExpr )*
            {
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:28:14: ( AND ^ orExpr )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==AND) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:28:15: AND ^ orExpr
            	    {
            	    AND3=(Token)match(input,AND,FOLLOW_AND_in_andExpr134); 
            	    AND3_tree = 
            	    (CommonTree)adaptor.create(AND3)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(AND3_tree, root_0);


            	    pushFollow(FOLLOW_orExpr_in_andExpr137);
            	    orExpr4=orExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, orExpr4.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "andExpr"


    public static class orExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "orExpr"
    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:32:1: orExpr : ( atom ) ( ( OR ^ atom )* ) ;
    public final MobiloQueryGrammerParser.orExpr_return orExpr() throws RecognitionException {
        MobiloQueryGrammerParser.orExpr_return retval = new MobiloQueryGrammerParser.orExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token OR6=null;
        MobiloQueryGrammerParser.atom_return atom5 =null;

        MobiloQueryGrammerParser.atom_return atom7 =null;


        CommonTree OR6_tree=null;

        try {
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:33:2: ( ( atom ) ( ( OR ^ atom )* ) )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:33:4: ( atom ) ( ( OR ^ atom )* )
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:33:4: ( atom )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:33:5: atom
            {
            pushFollow(FOLLOW_atom_in_orExpr153);
            atom5=atom();

            state._fsp--;

            adaptor.addChild(root_0, atom5.getTree());

            }


            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:33:11: ( ( OR ^ atom )* )
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:33:12: ( OR ^ atom )*
            {
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:33:12: ( OR ^ atom )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==OR) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:33:13: OR ^ atom
            	    {
            	    OR6=(Token)match(input,OR,FOLLOW_OR_in_orExpr158); 
            	    OR6_tree = 
            	    (CommonTree)adaptor.create(OR6)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(OR6_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_orExpr161);
            	    atom7=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom7.getTree());

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "orExpr"


    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:36:1: atom : ( '(' ! andExpr ')' !| NAME );
    public final MobiloQueryGrammerParser.atom_return atom() throws RecognitionException {
        MobiloQueryGrammerParser.atom_return retval = new MobiloQueryGrammerParser.atom_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token char_literal8=null;
        Token char_literal10=null;
        Token NAME11=null;
        MobiloQueryGrammerParser.andExpr_return andExpr9 =null;


        CommonTree char_literal8_tree=null;
        CommonTree char_literal10_tree=null;
        CommonTree NAME11_tree=null;

        try {
            // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:37:2: ( '(' ! andExpr ')' !| NAME )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==8) ) {
                alt3=1;
            }
            else if ( (LA3_0==NAME) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }
            switch (alt3) {
                case 1 :
                    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:37:5: '(' ! andExpr ')' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal8=(Token)match(input,8,FOLLOW_8_in_atom177); 

                    pushFollow(FOLLOW_andExpr_in_atom180);
                    andExpr9=andExpr();

                    state._fsp--;

                    adaptor.addChild(root_0, andExpr9.getTree());

                    char_literal10=(Token)match(input,9,FOLLOW_9_in_atom182); 

                    }
                    break;
                case 2 :
                    // D:\\Projects\\Mobilo\\Samples\\MobiloQueryGrammer\\MobiloQueryGrammer.g:38:4: NAME
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    NAME11=(Token)match(input,NAME,FOLLOW_NAME_in_atom188); 
                    NAME11_tree = 
                    (CommonTree)adaptor.create(NAME11)
                    ;
                    adaptor.addChild(root_0, NAME11_tree);


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"

    // Delegated rules


 

    public static final BitSet FOLLOW_andExpr_in_start117 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_orExpr_in_andExpr129 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_AND_in_andExpr134 = new BitSet(new long[]{0x0000000000000120L});
    public static final BitSet FOLLOW_orExpr_in_andExpr137 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_atom_in_orExpr153 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_OR_in_orExpr158 = new BitSet(new long[]{0x0000000000000120L});
    public static final BitSet FOLLOW_atom_in_orExpr161 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_8_in_atom177 = new BitSet(new long[]{0x0000000000000120L});
    public static final BitSet FOLLOW_andExpr_in_atom180 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9_in_atom182 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NAME_in_atom188 = new BitSet(new long[]{0x0000000000000002L});

}