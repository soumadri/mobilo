package com.mobiloqueryengine.core;

import java.io.IOException;
import java.util.LinkedList;

import com.mobiloindexer.domain.DocumentReference;

public interface ISearchResultEvaluator {
	
	public LinkedList<DocumentReference> getSearchResults(String keyword) throws IOException, ClassNotFoundException;
	
	public LinkedList<DocumentReference> intersectResults(LinkedList<DocumentReference> resultSet1, LinkedList<DocumentReference> resultSet2);
	
	public LinkedList<DocumentReference> unionResults(LinkedList<DocumentReference> resultSet1, LinkedList<DocumentReference> resultSet2);
	
	public LinkedList<DocumentReference> finalize(LinkedList<DocumentReference> resultSet1);
	
}
