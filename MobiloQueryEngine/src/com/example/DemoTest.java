package com.example;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import org.antlr.runtime.RecognitionException;

import com.mobiloindexer.domain.DocumentReference;
import com.mobiloindexer.domain.Hit;
import com.mobiloqueryengine.core.MobiloQueryEngine;


public class DemoTest {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 * @throws RecognitionException 
	 */
	
	
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException, RecognitionException {
		
		MobiloQueryEngine queryEngine =  new MobiloQueryEngine();
		LinkedList<DocumentReference> result = queryEngine.evaluateQuery("law or rules");
		
		/*
		ISearchResultEvaluator searchResultEvaluator = new SearchResultEvaluatorImpl();

		System.out.println();
		LinkedList<DocumentReference> searchMatchList1 = searchResultEvaluator.getSearchResults("law");
		
		LinkedList<DocumentReference> searchMatchList2 = searchResultEvaluator.getSearchResults("cat");
		
		LinkedList<DocumentReference> unionResult = searchResultEvaluator.unionResults(searchMatchList1, searchMatchList2);*/
		
		//System.out.println(serializeResult(searchResultEvaluator.finalize(searchMatchList1)));
		//System.out.println(serializeResult(searchResultEvaluator.finalize(searchMatchList1)));
		
		System.out.println(serializeResult(result));
		
	}
	
	public static String serializeResult(LinkedList<DocumentReference> demoList){
		Iterator<DocumentReference> iterator = demoList.iterator();
		StringBuilder resultString = new StringBuilder();
		
		while (iterator.hasNext()) {
			
			DocumentReference documentRefrence= iterator.next();
			
			resultString.append("[\nDocID: " + documentRefrence.getDocid()+"\nNumber of hits: "+documentRefrence.getHitcount() +"\nHits:\n");
			for(Hit hit:documentRefrence.getHitlist()){
				resultString.append("start position: "+hit.getStartposition()+"  end position: "+hit.getEndposition()+"\n");
				
			}
			resultString.append("]\n");
		}
		
		return resultString.toString();
	}

}
