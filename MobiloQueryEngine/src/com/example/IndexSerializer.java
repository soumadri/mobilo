package com.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.LinkedList;

import com.mobiloindexer.domain.DocumentReference;
import com.mobiloindexer.domain.Hit;
import com.mobiloindexer.domain.PostingListReference;

public class IndexSerializer {
	public static HashMap<String,LinkedList<DocumentReference>> getSampleIndexObject(){
		
		HashMap<String,LinkedList<DocumentReference>> indexObject = new HashMap<String, LinkedList<DocumentReference>>();
	
		LinkedList<DocumentReference> docList = new LinkedList<DocumentReference>();
		
		docList.add(getDocRef("aaaabbbb1",2));
		docList.add(getDocRef("aaaabbbb2",6));
		docList.add(getDocRef("aaaabbbb3",8));
		docList.add(getDocRef("aaaabbbb4",5));
		docList.add(getDocRef("aaaabbbb5",4));
		
		indexObject.put("cat", docList);

		LinkedList<DocumentReference> docList2 = new LinkedList<DocumentReference>();
		
		docList2.add(getDocRef("aaaabbbb1",3));
		docList2.add(getDocRef("aaaabbbb6",5));
		docList2.add(getDocRef("aaaabbbb3",1));
		docList2.add(getDocRef("aaaabbbb7",10));
		docList2.add(getDocRef("aaaabbbb5",7));
		
		indexObject.put("dog",docList2);
		
		LinkedList<DocumentReference> docList3 = new LinkedList<DocumentReference>();
		
		docList3.add(getDocRef("aaaabbbb8",8));
		docList3.add(getDocRef("aaaabbbb6",3));
		docList3.add(getDocRef("aaaabbbb9",7));
		docList3.add(getDocRef("aaaabbbb7",9));
		docList3.add(getDocRef("aaaabbbb10",1));
		
		indexObject.put("lion",docList3);
		
		return indexObject;

	}
	
	public static DocumentReference getDocRef(String docid, int hitCount){
		DocumentReference dr = new DocumentReference();
		dr.setDocid(docid);
		LinkedList<Hit> hll = new LinkedList<Hit>();
		
		for(int i=0;i<hitCount;i++){
			Hit hit =  new Hit();
			hit.setStartposition(100);
			hit.setEndposition(106);
			hll.add(hit);
		}
		
		
		dr.setHitcount(hitCount);
		dr.setHitlist(hll);
		
		return dr;
	}
	
	public static Boolean serializeDictioneryObject(HashMap<String, PostingListReference> dictionery) throws FileNotFoundException, IOException{
		File file = new File("C:\\index.ser");
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(dictionery);
		
		oos.close();
		fos.close();
		
		return null;
	}
}
