## What is Mobilo? ##
Mobilo is an offline search engine for ereader applications (primarily for mobile devices).

## What platforms does it support? ##
Mobilo search library is available for iOS and Android. However, since the Android port is written in core java, it can be used any java based application. The search index generator is an java application available in command-line as well as a REST based web app flavors.

## What is the licensing model of Mobilo? ##
Mobilo is licensed under the [Modified BSD License](http://opensource.org/licenses/BSD-3-Clause), which is GPL compatible. The licenses for the  third-party components are listed below, and the usage of the same is governed by the respective licenses,

* Stanford CoreNLP - [GPL v2](http://www.gnu.org/licenses/gpl-2.0.html)
* PorterStemmer - free to use and modify. 
* ANTLR Runtime - [BSD License](http://www.antlr.org/license.html)

## What third-party component does Mobilo use? ##
Mobilo uses several third-party components,

* [Stanford CoreNLP](http://nlp.stanford.edu/software/corenlp.shtml) - used to tokenize the input text.
* [Porter stemmer](http://tartarus.org/martin/PorterStemmer/java.txt) - used as a word stemmer in the search library
* [ANTLR Runtime](http://www.antlr.org/) - used to parse search query. An [Objective-C port](https://github.com/muggins/ANTLR3-ObjC2.0-Runtime) of the same is used for Mobilo search library for iOS.

## Documentation ##
* [API documentation](https://bitbucket.org/soumadri/mobilo/wiki/Mobil%20API%20Specification)
* Design document